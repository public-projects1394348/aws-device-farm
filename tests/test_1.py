
import unittest
from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from appium.options.android import UiAutomator2Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#@unittest.skip("skip") 
class TestCase1(unittest.TestCase):
    
    @classmethod
    def setUpClass(self) -> None:
        # local_run_capabilities = dict(
        #     platformName='Android',
        #     automationName='uiautomator2',
        #     deviceName='emulator-5554',
        #     app='/Users/ali/Documents/bitbar-sample-app.apk',
        # )
        desired_capabilities = {} # AWS replaces these values to devices it runs
        capabilities_options = UiAutomator2Options().load_capabilities(desired_capabilities)
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', options=capabilities_options) # AWS uses 1.x path as base path also 2.x runs
    
    @classmethod
    def tearDownClass(self) -> None:
        if self.driver:
            self.driver.quit()    
    
    def test_0_first_view(self) -> None:
        text1 = self.driver.find_element(AppiumBy.ID, "com.bitbar.testdroid:id/radio2").text
        self.assertIn("Ask mom for help", text1)
        self.driver.save_screenshot('img.png')
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCase1)
    unittest.TextTestRunner(verbosity=2).run(suite)